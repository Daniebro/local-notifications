//
//  ViewController.swift
//  Project21
//
//  Created by Danni Brito on 2/16/20.
//  Copyright © 2020 Danni Brito. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController, UNUserNotificationCenterDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Register", style: .plain, target: self, action: #selector(registerLocal))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Schedule", style: .plain, target: self, action: #selector(scheduleLocal))
    }
    
    @objc func registerLocal(){
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            if granted {
                print("yay!")
            } else {
                print("D'oh!")
            }
        }
    }
    
    @objc func scheduleLocal(sender: Any){
        registerCategories()
        var timeInterval = 0.0
        if let _ = sender as? UIBarButtonItem {
            timeInterval = 5.0
        } else {
            timeInterval = 86400.0
        }
        
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
        
        let content = UNMutableNotificationContent()
        content.title = "Late wake up call"
        content.body = "The early bird cacthes de worm, but the second mouse gets the cheese"
        content.categoryIdentifier = "alarm"
        content.userInfo = ["custom": "fizzbuzz"]
        content.sound = .default
        
        // give the alarm the exact time
//        var dateComponents = DateComponents()
//        dateComponents.hour = 10
//        dateComponents.minute = 30
//        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterval, repeats: false )
        
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        center.add(request)
    }
    
    
    func registerCategories() {
        let center = UNUserNotificationCenter.current()
        center.delegate = self

        let show = UNNotificationAction(identifier: "show", title: "Tell me more…", options: .foreground)
        let oneDay = UNNotificationAction(identifier: "oneDayLater", title: "Remind Me Tomorrow", options: .foreground)
        let category = UNNotificationCategory(identifier: "alarm", actions: [show, oneDay], intentIdentifiers: [])

        center.setNotificationCategories([category])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // pull out the buried userInfo dictionary
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)

        var identifier = ""
        if let customData = userInfo["custom"] as? String {
            print("Custom data received: \(customData)")
            
            switch response.actionIdentifier {
            case UNNotificationDefaultActionIdentifier:
                // the user swiped to unlock
                print("Default identifier")
                identifier = UNNotificationDefaultActionIdentifier.description

            case "show":
                // the user tapped our "show more info…" button
                print("Show more information…")
                identifier = "Show more info Identifier"
            case "oneDayLater":
                scheduleLocal(sender: "")
            default:
                identifier = "User touch the alert"
                break
            }
            
        }
        let ac = UIAlertController(title: "Identifier passed in", message: identifier, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(ac, animated: true, completion: nil)

        // you must call the completion handler when you're done
        completionHandler()
    }
    
    

}

